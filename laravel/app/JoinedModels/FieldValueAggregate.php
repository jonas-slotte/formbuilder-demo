<?php

namespace App\JoinedModels;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Virtual model focused around the 
 */
class FieldValueAggregate extends Model
{
    use HasFactory;

    public function formFieldInstances()
    {
        return $this->belongsTo(FormFieldInstance::class);
    }
}
