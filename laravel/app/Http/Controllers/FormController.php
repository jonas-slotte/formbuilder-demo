<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Core\Form;
use JonasSlotte\FormBuilder\Services\LaravelFormBuilder;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FormController extends Controller
{
    protected function getDefaultData()
    {
        return [
            'first' => [
                'first_field_id' => 'defaultval'
            ],
            'contact' => [
                'name' => 'defaultName',
                'email' => 'default@email.com',
                'message' => 'Some message yes'
            ]
        ];
    }

    protected function getForms()
    {
        return [
            'first' => [
                'name' => "FirstForm",
                'fields' => [[
                    'id' => 'first_field_id',
                    'type' => "text",
                    'name' => "FirstField",
                    'validation' => [
                        'required' => true
                    ]
                ]]
            ],
            'contact' => [
                'name' => "ContactForm",
                'fields' => [
                    [
                        'id' => 'name',
                        'type' => "text",
                        'name' => "Name",
                        'validation' => [
                            'required' => true
                        ]
                    ],
                    [
                        'id' => 'email',
                        'type' => "email",
                        'name' => "Email",
                        'validation' => [
                            'required' => true
                        ]
                    ],
                    [
                        'id' => 'message',
                        'type' => "textarea",
                        'name' => "Message",
                        'validation' => [
                            'required' => true
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Get a registered form instance
     *
     * @param string $form
     */
    protected function getForm($formKey)
    {
        $forms = $this->getForms();
        if (in_array($formKey, array_keys($forms))) {
            /** @var Form $form */
            $builder = new LaravelFormBuilder();
            $defaultData = $this->getDefaultData()[$formKey] ?? [];
            $form = $builder->form($forms[$formKey], $defaultData);
            return ["form" => $form, "session" => $builder->getSessionValueSource()->toArray(), "santitized" => [], "default" => $defaultData];
        }
        throw new NotFoundHttpException("Form $formKey could not be found.");
    }

    public function index()
    {
        return view('welcome', ['forms' => array_keys($this->getForms())]);
    }

    public function store(Request $request, string $formKey)
    {
        $validator = $this->getForm($formKey)["form"]->makeValidator($request->all());
        $validator->validate();
        return redirect()->back()->withInput()->with('success', 'ok');
    }

    public function show(Request $request, $formKey)
    {
        $data = $this->getForm($formKey);
        return view('form', [
            'forms' =>  array_keys($this->getForms()),
            'form' => $data["form"],
            'session' => $data["session"],
            'default' => $data["default"],
            'currentFormKey' => $formKey
        ]);
    }
}
