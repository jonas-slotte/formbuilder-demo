<?php

namespace Database\Seeders;

use App\Models\FieldType;
use App\Models\FormType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        FormType::create([
            'key' => 'default',
            'name' => "The default form type"
        ]);
        FieldType::create([
            'key' => 'text',
            'name' => "A text field"
        ]);
        FieldType::create([
            'key' => 'option',
            'name' => "The type that is used to define an option field"
        ]);

        Model::reguard();
    }
}
