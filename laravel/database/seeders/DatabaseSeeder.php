<?php

namespace Database\Seeders;

use App\Models\BooleanValue;
use App\Models\ValueRoot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        Model::unguard();

        $uuid = uuidString();
        ValueRoot::create([
            'uuid' => $uuid,
        ]);
        BooleanValue::create([
            'uuid' => $uuid,
            'value' => false
        ]);
    }
}
