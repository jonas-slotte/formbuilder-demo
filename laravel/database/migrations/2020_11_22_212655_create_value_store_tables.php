<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValueStoreTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * This stores exactly one value of one type. No fancy tricks here.
         */
        Schema::create('value_roots', function (Blueprint $table) {
            $table->uuid('uuid')->primary();

            //Morph reverse, only used occasionally
            $table->string("type");
            $table->index(["type", "uuid"], 'value_type_uuid');
            /**
             * Add fields here if any value needs more data
             */
            $table->timestamps();
        });

        /**
         * Store boolean values
         */
        Schema::create('boolean_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->boolean('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });

        /**
         * Store short string values
         */
        Schema::create('string_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });

        /**
         * Store regular text values
         */
        Schema::create('text_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->text('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });

        /**
         * Store big signed integer values
         */
        Schema::create('integer_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->bigInteger('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });

        /**
         * Store date strings
         */
        Schema::create('date_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->date('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });

        /**
         * Store datetime () strings
         */
        Schema::create('datetime_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->dateTime('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });

        /**
         * Store time () fields
         */
        Schema::create('time_values', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->time('value');
            $table->foreign('uuid')->references('uuid')->on('value_roots')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_values');
        Schema::dropIfExists('datetime_values');
        Schema::dropIfExists('date_values');
        Schema::dropIfExists('integer_values');
        Schema::dropIfExists('string_values');
        Schema::dropIfExists('text_values');
        Schema::dropIfExists('boolean_values');
    }
}
