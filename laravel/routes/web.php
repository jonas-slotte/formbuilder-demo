<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => '\App\Http\Controllers'], function () {
    Route::get('/', 'FormController@index')->name('home');
    Route::post('/{form}', 'FormController@store')->name('formpost');
    Route::get('/{form}', 'FormController@show')->name('form');
});
