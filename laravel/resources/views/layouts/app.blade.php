<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form builder</title>
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
    <script src="{{mix('js/app.js')}}"></script>
    <style>
        html {
            height: 100%;
        }
    </style>
</head>

<body class="bg-dark h-100">
    <nav class="navbar navbar-expand-lg navbar-dark ">
        <a class="navbar-brand" href="/">Start</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                @foreach($forms as $form)
                <li class="nav-item {{($currentFormKey ?? '') == $form ? 'active': ''}}">
                    <a class="nav-link" href="{{route('form',$form)}}">
                        {{ucfirst($form)}}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </nav>
    @yield('content')
</body>

</html>