@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-12 col-lg-8 col-md-10 col-xl-6 mx-auto">
      <div class="card m-2">
        <div class="card-header">
          {{$form->name()}}
        </div>
        <div class="card-body">
          @yield('formcontent')
        </div>
      </div>
      <div class="card m-2">
        <div class="card-header">
          Session
        </div>
        <div class="card-body">
          @php dump($session) @endphp
        </div>
      </div>

      <div class="card m-2">
        <div class="card-header">
          Default
        </div>
        <div class="card-body">
          @php dump($default) @endphp
        </div>
      </div>
    </div>
  </div>
</div>
@endsection