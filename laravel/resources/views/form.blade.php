@extends('layouts.form')

@section('formcontent')
<form method="POST" action="{{route('formpost',$currentFormKey)}}">
  @csrf
  @foreach($form->fieldViews() as $field)
  @include($field->bladeKey())
  @endforeach
  <button type="submit" class="btn btn-primary">Submit</button>
 
  @if ($errors->any())
      <div class="alert alert-danger mt-3">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  @if(session()->has('success'))

    <div class="alert alert-success mt-3">
  OK
    </div>
  @endif
</form>
@endsection