<?php

namespace Tests\Feature;

use App\Core\Field;
use App\Core\FieldOption;
use App\Core\Form;
use App\Repositories\FormRepository;
use App\Services\FormBuilder;
use Tests\FeatureTestCase;

class FormRepositoryTest extends FeatureTestCase
{
    /**
     * @return FormRepository
     */
    protected function makeRepository()
    {
        return app()->make(FormRepository::class);
    }
    /**
     * @return void
     */
    public function testPersist()
    {
        /**
         * Use formbuilder to make a somewhat complex form we can persist
         */
        $builder = new FormBuilder();

        // $optionField = new Field(
        //     $resolver->identity(1, "fieldname", "fieldkey", 1),
        //     $resolver->fieldType('text'),
        //     $resolver->validationCollection(),
        //     $resolver->fieldOptionCollection()
        // );
        // $optionFields =  $resolver->fieldCollection([
        //     $optionField
        // ]);
        // $fieldOption = new FieldOption(
        //     $resolver->identity(2, "fieldoption", "fieldoptionkey", 1),
        //     $optionFields
        // );
        // $fieldOptions = $resolver->fieldOptionCollection([
        //     $fieldOption
        // ]);
        // $formField = new Field(
        //     $resolver->identity(3, "fieldname", "fieldkey", 1),
        //     $resolver->fieldType('option'),
        //     $resolver->validationCollection(),
        //     $fieldOptions
        // );
        $form = $builder->form([
            'id' => 1,
            "name" => "formname",
            "key" => "formkey",
            "version" => 1,
            "fields" => [
                [
                    'id' => 3,
                    "name" => "fieldname",
                    "key" => "fieldkey",
                    "version" => 1,
                    "type" => "option",
                    "options" => [
                        [
                            'id' => 2,
                            "name" => "fieldoption",
                            "key" => "fieldoptionkey",
                            "version" => 1,
                            "fields" => [
                                [
                                    'id' => 1,
                                    "name" => "fieldname",
                                    "key" => "fieldkey",
                                    "version" => 1,
                                    "type" => "text",
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // $formFields = $resolver->fieldCollection([
        //     $formField
        // ]);
        // $form = new Form(
        //     $resolver->identity(1, "formname", "formkey", 1),
        //     $resolver->formType('default'),
        //     $formFields
        // );



        $repo = $this->makeRepository();
        $repo->persist($form);

        $formField = $form->fields()->first();
        $fieldOption = $formField->options()->first();
        $optionField = $fieldOption->fields()->first();

        $this->assertDatabaseHas('forms', [
            'id' => $form->id()->get(),
            'key' => $form->key()->get(),
            'name' => $form->name()->get(),
            'version' => $form->version()->get(),
        ]);
        $this->assertDatabaseHas('fields', [
            'id' => $formField->id()->get(),
            'key' => $formField->key()->get(),
            'name' => $formField->name()->get(),
            'version' => $formField->version()->get(),
        ]);
        $this->assertDatabaseHas('form_fields', [
            'form_id' => $form->id()->toInt(),
            'field_id' => $formField->id()->toInt(),
        ]);
        $this->assertDatabaseHas('fields', [
            'id' => $fieldOption->id()->toInt(),
            'key' => $fieldOption->key()->get(),
            'name' => $fieldOption->name()->get(),
            'version' => $fieldOption->version()->get(),
            'parent_field_id' => $formField->id()->toInt()
        ]);

        //Assert that a re-fetched intance is complete (ids)
        $found = $repo->find($form->id());
        $this->assertEquals(true, $found->is($form));
        $foundField = $found->fields()->first();
        $this->assertEquals(true, $foundField->is($formField));
        $foundOption = $foundField->options()->first();
        $this->assertEquals(true, $foundOption->is($fieldOption));
        $foundOptionField = $foundOption->fields()->first();
        $this->assertEquals(true, $foundOptionField->is($optionField));
    }
}
