<?php

namespace Tests\Feature;

use App\Services\LaravelFormBuilder;
use Illuminate\Validation\ValidationException;
use Tests\FeatureTestCase;

class FormBuilderServiceTest extends FeatureTestCase
{
  /**
   * Test the form service can make forms as easily as possible by default.
   *
   * @return void
   */
  public function testMakeFormWithService()
  {
    $srv = new LaravelFormBuilder();
    $form = $srv->form([
      'fields' => [
        [
          'id' => 'the_field_id',
          'type' => 'text',
          'name' => 'A text field',
          'validation' => ['required']
        ]
      ]
    ]);
    try {
      $form->validate([]);
      $this->fail("Should throw exception");
    } catch (ValidationException $x) {
      $this->assertEquals(__('validation.required', ['attribute' => 'A text field']), $x->errors()["the_field_id"]);
    }
  }
}
