<?php

namespace Tests\Unit;

use App\Core\Id;
use App\ValueSources\ArrayValueSource;
use App\ValueSources\MultiValueSource;
use Tests\UnitTestCase;

class ValueSourceTest extends UnitTestCase
{
  public function testMultiValueSourcePriority()
  {
    $multi = new MultiValueSource();
    $zero = new ArrayValueSource();
    $one = new ArrayValueSource();
    $two = new ArrayValueSource();
    $id = new Id("one");
    $multi->register('one', $one);
    $multi->register('two', $two);

    // $zero->put($id, "");
    $one->put($id, "theone");
    $two->put($id, "thetwo");

    $this->assertEquals(null, $zero->get($id));
    $this->assertEquals("theone", $one->get($id));
    $this->assertEquals("thetwo", $two->get($id));

    $this->assertEquals("theone", $multi->get($id), "Should be the first source's value");
  }
}
