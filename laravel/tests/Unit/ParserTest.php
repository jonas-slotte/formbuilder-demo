<?php

namespace Tests\Unit;

use App\Core\FormType;
use App\Core\Parser;
use App\FieldTypes\FieldTypeRegistry;
use App\FieldTypes\Text\TextType;
use App\FormTypes\FormTypeRegistry;
use Tests\UnitTestCase;

class ParserTest extends UnitTestCase
{
    /**
     * Test it passes the parser
     * 
     * @return void
     */
    public function testParser()
    {
        $formTypes = new FormTypeRegistry();
        $formTypes->register('formtype', new FormType('formtype'));
        $fieldTypes = new FieldTypeRegistry();
        $fieldTypes->register('fieldtype', new TextType('fieldtype'));
        $fieldTypes->register('optionfieldtype', new TextType('optionfieldtype'));

        $parser = new Parser($formTypes, $fieldTypes);
        $form = $parser->form([
            'id' => "formid",
            'key' => 'testform',
            'name' => 'formname',
            'type' => 'formtype',
            'fields' => [
                [
                    'id' => 'fieldid',
                    'key' => 'testfield',
                    'name' => 'fieldname',
                    'type' => 'fieldtype',
                    'options' => [
                        [
                            'id' => 'optionid',
                            'key' => 'optionkey',
                            'name' => 'optionname',
                            'fields' => [
                                [
                                    'id' => 'optionfieldid',
                                    'key' => 'optionfield',
                                    'name' => 'optionfieldname',
                                    'type' => 'optionfieldtype'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->assertEquals('formid', $form->id()->get());
        $this->assertEquals('formname', $form->name()->get());
        $this->assertEquals('formtype', $form->type()->get());
        $this->assertEquals('testform', $form->key()->get());

        $this->assertEquals(1, sizeof($form->fields()));
        $this->assertEquals('fieldid', $form->fields()->get(0)->id());
        $this->assertEquals('optionid', $form->fields()->get(0)->options()->get(0)->id());
        $this->assertEquals('optionfieldid', $form->fields()->get(0)->options()->get(0)->fields()->get(0)->id());
    }
}
