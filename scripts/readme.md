## dev readme
The dev script delegates commands to scripts folder.

# gcr
For pushing this repo to Google Container Registry (Need GCR utils installed and authenticated)

# dc
This command delegates docker-compose commands for local development configuration
- up
    Starts everything
- down
    Stops everything
- nuke
    Remove local stray volumes
- w
    Get shell into workspace for doing commands
- docs
    Build html documentation from source in the docs directory (Open the docs/build/html/index.html)