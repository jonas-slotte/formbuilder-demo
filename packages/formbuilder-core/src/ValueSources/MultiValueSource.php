<?php

namespace JonasSlotte\FormBuilder\ValueSources;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\ValueSource;
use JonasSlotte\FormBuilder\Core\ValueType;

class MultiValueSource extends ValueSource
{
  /**
   * @var ValueSource[]
   */
  protected $sources = [];

  /**
   * Register a new
   *
   * @param string $key
   * @param ValueSource $type
   * @return void
   */
  public function register(string $key, ValueSource $type)
  {
    $this->sources[$key] = $type;
  }

  /**
   * @param string $key
   * @return ValueSource
   */
  public function getSource(string $key)
  {
    return $this->types[$key];
  }

  public function put(Id $id, $mixed)
  {
    //Nop
  }

  /**  Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public function exists(Id $id)
  {
    foreach ($this->sources as $source) {
      if ($source->exists($id)) {
        return true;
      }
    }
    return false;
  }

  /**  Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public function get(Id $id)
  {
    foreach ($this->sources as $source) {
      if ($source->exists($id)) {
        return $source->get($id);
      }
    }
    return null;
  }

  /**
   * Get a string value from the source by id
   *
   * @param Id $id
   * @return string
   */
  public function getString(Id $id)
  {
    foreach ($this->sources as $srcKey => $source) {
      if ($source->exists($id)) {
        return $source->getString($id);
      }
    }
    return "";
  }
}
