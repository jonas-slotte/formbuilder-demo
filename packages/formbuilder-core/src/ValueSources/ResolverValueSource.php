<?php

namespace JonasSlotte\FormBuilder\ValueSources;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\ValueSource;
use JonasSlotte\FormBuilder\Core\ValueType;
use JonasSlotte\FormBuilder\ValueResolvers\ValueResolver;
use Illuminate\Support\Collection;

/**
 * Generic value source
 */
class ResolverValueSource extends ValueSource
{
  /**
   * @var ValueResolver
   */
  protected $source;

  public function __construct(ValueResolver $source)
  {
    $this->source = $source;
  }

  /**
   * Put a value
   *
   * @param Id $id
   * @param mixed $value
   * @return void
   */
  public function put(Id $id, $value)
  {
  }

  /**
   * Check if key exists
   *
   * @param Id $id
   * @return true
   */
  public function exists(Id $id)
  {
  }

  /**
   * Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public function get(Id $id)
  {
  }

  /**
   * Get a string value from the source by id
   *
   * @param Id $id
   * @return string
   */
  public function getString(Id $id)
  {
    return $this->source->getStringValue();
  }

  /**
   * Get int value from the source by id
   *
   * @param Id $id
   * @return int
   */
  public function getInt(Id $id)
  {
  }

  /**
   * Get bool value from the source by id
   *
   * @param Id $id
   * @return bool
   */
  public function getBool(Id $id)
  {
  }
}
