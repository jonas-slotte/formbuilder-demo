<?php

namespace JonasSlotte\FormBuilder\ValueSources;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\ValueSource;
use JonasSlotte\FormBuilder\Core\ValueType;
use Illuminate\Support\Collection;

/**
 * Generic value source
 */
class BaseValueSource extends ValueSource
{
  /**
   * @var Collection
   */
  protected $valueMap;

  public function __construct()
  {
    $this->valueMap = new Collection();
  }

  /**
   * Put a value
   *
   * @param Id $id
   * @param mixed $value
   * @return void
   */
  public function put(Id $id, $value)
  {
    $this->valueMap->put($id->get(), $value);
  }

  /**
   * Check if key exists
   *
   * @param Id $id
   * @return true
   */
  public function exists(Id $id)
  {
    return $this->valueMap->offsetExists($id->get());
  }

  /**
   * Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public function get(Id $id)
  {
    return $this->valueMap->get($id->get());
  }

  /**
   * Get a string value from the source by id
   *
   * @param Id $id
   * @return string
   */
  public function getString(Id $id)
  {
    return (string) $this->get($id);
  }

  /**
   * Get int value from the source by id
   *
   * @param Id $id
   * @return int
   */
  public function getInt(Id $id)
  {
    return (int) $this->get($id);
  }

  /**
   * Get bool value from the source by id
   *
   * @param Id $id
   * @return bool
   */
  public function getBool(Id $id)
  {
    return (bool) $this->get($id);
  }
}
