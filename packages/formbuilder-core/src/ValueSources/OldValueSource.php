<?php

namespace JonasSlotte\FormBuilder\ValueSources;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\ValueSource;
use JonasSlotte\FormBuilder\Core\ValueType;
use JonasSlotte\FormBuilder\ValueResolvers\ValueResolver;
use Illuminate\Session\Store;
use Illuminate\Support\Collection;

/**
 * session
 */
class OldValueSource extends ValueSource
{
  /**
   * @var Store
   */
  protected $session;

  public function __construct(Store $session)
  {
    $this->session = $session;
  }

  /**
   * Put a value
   *
   * @param Id $id
   * @param mixed $value
   * @return void
   */
  public function put(Id $id, $value)
  {
    //Set by framework session
  }

  /**
   * Check if key exists
   *
   * @param Id $id
   * @return true
   */
  public function exists(Id $id)
  {
    return $this->session->hasOldInput($id->get());
  }

  /**
   * Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public function get(Id $id)
  {
    return $this->session->getOldInput($id->get());
  }

  /**
   * Get a string value from the source by id
   *
   * @param Id $id
   * @return string
   */
  public function getString(Id $id)
  {
    return $this->session->getOldInput($id->get(), "");
  }

  /**
   * Get int value from the source by id
   *
   * @param Id $id
   * @return int
   */
  public function getInt(Id $id)
  {
    return $this->session->getOldInput($id->get(), 0);
  }

  /**
   * Get bool value from the source by id
   *
   * @param Id $id
   * @return bool
   */
  public function getBool(Id $id)
  {
    return $this->session->getOldInput($id->get(), false);
  }

  public function toArray()
  {
    return $this->session->getOldInput(null, []);
  }
}
