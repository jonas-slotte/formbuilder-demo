<?php

namespace JonasSlotte\FormBuilder\ValueSources;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\ValueSource;
use JonasSlotte\FormBuilder\Core\ValueType;
use JonasSlotte\FormBuilder\ValueResolvers\ValueResolver;
use Illuminate\Session\Store;
use Illuminate\Support\Collection;

/**
 * session
 */
class SessionValueSource extends ValueSource
{
  /**
   * @var Store
   */
  protected $session;

  /**
   * @var string
   */
  protected $prefix;

  public function __construct(Store $session, string $prefix = "")
  {
    $this->session = $session;
    $this->prefix =  $prefix;
  }

  protected function applyPrefix(Id $id)
  {
    return  $this->prefix . "_" . $id->get();
  }

  /**
   * Put a value
   *
   * @param Id $id
   * @param mixed $value
   * @return void
   */
  public function put(Id $id, $value)
  {
    $this->session->put($this->applyPrefix($id), $value);
  }

  /**
   * Check if key exists
   *
   * @param Id $id
   * @return true
   */
  public function exists(Id $id)
  {
    return $this->session->exists($this->applyPrefix($id));
  }

  /**
   * Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public function get(Id $id)
  {
    return $this->session->get($this->applyPrefix($id));
  }

  /**
   * Get a string value from the source by id
   *
   * @param Id $id
   * @return string
   */
  public function getString(Id $id)
  {
    return $this->session->get($this->applyPrefix($id), "");
  }

  /**
   * Get int value from the source by id
   *
   * @param Id $id
   * @return int
   */
  public function getInt(Id $id)
  {
    return $this->session->get($this->applyPrefix($id), 0);
  }

  /**
   * Get bool value from the source by id
   *
   * @param Id $id
   * @return bool
   */
  public function getBool(Id $id)
  {
    return $this->session->get($this->applyPrefix($id), false);
  }

  public function toArray()
  {
    throw new \Exception("Session value source cannot produce array of values.");
  }
}
