<?php

namespace JonasSlotte\FormBuilder\ValueTypes;

use JonasSlotte\FormBuilder\Core\ValueType;

class ValueTypeRegistry
{
  /**
   * @var array
   */
  protected $types = [];

  public function registerClass(string $key, string $class)
  {
    $this->register($key, new $class($key));
  }

  public function register(string $key, ValueType $type)
  {
    $this->types[$key] = $type;
  }

  /**
   * @param string $key
   * @return ValueType
   */
  public function get(string $key)
  {
    return $this->types[$key];
  }
}
