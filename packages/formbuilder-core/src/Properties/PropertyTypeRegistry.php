<?php

namespace JonasSlotte\FormBuilder\Properties;

use JonasSlotte\FormBuilder\Core\PropertyType;

class PropertyTypeRegistry
{
    /**
     * @var array
     */
    protected $types = [];

    public function registerClass(string $key, string $class)
    {
        $this->register($key, new $class($key));
    }

    public function register(string $key, PropertyType $type)
    {
        $this->types[$key] = $type;
    }

    /**
     * @param string $key
     * @return PropertyType
     */
    public function get(string $key)
    {
        return $this->types[$key];
    }
}
