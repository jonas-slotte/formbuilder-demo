<?php

namespace JonasSlotte\FormBuilder\Core;

class FormInstance
{
    /**
     * @var Field
     */
    protected $form;

    /**
     * @var FieldInstanceCollection
     */
    protected $fields;

    /**
     * @var Id
     */
    protected $id;

    public function __construct(Id $id, Form $form, FieldInstanceCollection $fields)
    {
        $this->form = $form;
        $this->fields = $fields;
        $this->id = $id;
    }

    public function id()
    {
        return $this->id;
    }

    public function form()
    {
        return $this->form;
    }

    public function fields()
    {
        return $this->fields;
    }
}
