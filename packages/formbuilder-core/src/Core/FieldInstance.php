<?php

namespace JonasSlotte\FormBuilder\Core;

class FieldInstance
{
    /**
     * @var Field
     */
    protected $field;

    /**
     * @var Id
     */
    protected $id;

    /**
     * @var ValueSource
     */
    protected $valueSource;


    public function __construct(Field $field, Id $id, ValueSource $valueSource)
    {
        $this->field = $field;
        $this->id = $id;
        $this->valueSource = $valueSource;
    }

    public function id()
    {
        return $this->id;
    }

    public function field()
    {
        return $this->field;
    }

    public function valueSource()
    {
        return $this->valueSource;
    }
}
