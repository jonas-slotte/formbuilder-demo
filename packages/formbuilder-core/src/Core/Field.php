<?php

namespace JonasSlotte\FormBuilder\Core;

class Field
{
    /**
     * @var Id
     */
    protected $identity;

    /**
     * @var FieldType
     */
    protected $type;

    /**
     * @var FieldOptionCollection
     */
    protected $options;

    /**
     * @var PropertyCollection
     */
    protected $properties;

    public function __construct(Id $identity,  FieldType $formType, PropertyCollection $properties, FieldOptionCollection $options)
    {
        $this->identity = $identity;
        $this->type = $formType;
        $this->options = $options;
        $this->properties = $properties;
    }

    public function is(Field $other)
    {
        return $this->id()->is($other->id());
    }

    public function id()
    {
        return $this->identity;
    }

    public function type()
    {
        return $this->type;
    }

    public function properties()
    {
        return $this->properties;
    }

    public function options()
    {
        return $this->options;
    }
}
