<?php

namespace JonasSlotte\FormBuilder\Core;

class Property
{
    /**
     * @var Id
     */
    protected $identity;

    /**
     * @var PropertyType
     */
    protected $type;

    /**
     * @var array
     */
    protected $data;

    public function __construct(Id $identity,  PropertyType $type, array $data)
    {
        $this->identity = $identity;
        $this->type = $type;
        $this->data = $data;
    }

    public function is(Property $other)
    {
        return $this->id()->is($other->id());
    }

    public function id()
    {
        return $this->identity;
    }

    public function type()
    {
        return $this->type;
    }

    /**
     * Get the payload of the property
     *
     * @return array
     */
    public function getPayload()
    {
        return $this->data;
    }
}
