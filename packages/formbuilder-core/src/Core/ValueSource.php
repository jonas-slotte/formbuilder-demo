<?php

namespace JonasSlotte\FormBuilder\Core;

abstract class ValueSource
{
  /**
   * Put a value
   *
   * @param Id $id
   * @param mixed $value
   * @return void
   */
  public abstract function put(Id $id, $value);

  /**
   * Check if key exists
   *
   * @param Id $id
   * @return true
   */
  public abstract function exists(Id $id);

  /**
   * Get any value from the source by id
   *
   * @param Id $id
   * @return mixed
   */
  public abstract function get(Id $id);

  /**
   * Get a string value from the source by id
   *
   * @param Id $id
   * @return string
   */
  public function getString(Id $id)
  {
    return (string)$this->get($id);
  }

  /**
   * Get int value from the source by id
   *
   * @param Id $id
   * @return int
   */
  public function getInt(Id $id)
  {
    return (int)$this->get($id);
  }

  /**
   * Get bool value from the source by id
   *
   * @param Id $id
   * @return bool
   */
  public function getBool(Id $id)
  {
    return (bool)$this->get($id);
  }
}
