<?php

namespace JonasSlotte\FormBuilder\Core;

use Ramsey\Uuid\Uuid;

class Id
{
    protected $id;

    public function __construct($input)
    {
        $this->id = $input;
    }

    public function is(Id $other)
    {
        return $this->get() === $other->get();
    }

    public function get()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string) $this->get();
    }

    public static function generate()
    {
        return new static(Uuid::uuid4()->toString());
    }

    public function toInt()
    {
        return (int) $this->get();
    }
}
