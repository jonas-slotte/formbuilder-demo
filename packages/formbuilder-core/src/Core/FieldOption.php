<?php

namespace JonasSlotte\FormBuilder\Core;

class FieldOption
{
    /**
     * @var Id
     */
    protected $identity;

    /**
     * @var FieldCollection
     */
    protected $fields;

    public function __construct(Id $identity, FieldCollection $fields)
    {
        $this->identity = $identity;
        $this->fields = $fields;
    }

    public function is(FieldOption $other)
    {
        return $this->id()->is($other->id());
    }

    public function id()
    {
        return $this->identity;
    }

    public function fields()
    {
        return $this->fields;
    }
}
