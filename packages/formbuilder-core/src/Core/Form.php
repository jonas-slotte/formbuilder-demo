<?php

namespace JonasSlotte\FormBuilder\Core;

class Form
{
    /**
     * @var Id
     */
    protected $identity;

    /**
     * @var FormType
     */
    protected $type;

    /**
     * @var FieldCollection
     */
    protected $fields;

    public function __construct(Id $identity, FormType $formType, FieldCollection $fields)
    {
        $this->identity = $identity;
        $this->type = $formType;
        $this->fields = $fields;
    }

    public function is(Form $other)
    {
        return $this->id()->is($other->id());
    }

    public function id()
    {
        return $this->identity;
    }

    public function type()
    {
        return $this->type;
    }

    public function fields()
    {
        return $this->fields;
    }

    public function view()
    {
        return $this->type()->makeView($this);
    }
}
