<?php

namespace JonasSlotte\FormBuilder\Core;

abstract class FieldType
{
    protected $type;

    public function __construct($input)
    {
        $this->type = $input;
    }

    public function get()
    {
        return $this->type;
    }

    public function defaultRules()
    {
        return [];
    }

    public abstract function makeView(FieldInstance $inst);

    public function __toString()
    {
        return (string) $this->get();
    }
}
