<?php

namespace JonasSlotte\FormBuilder\Services;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldCollection;
use JonasSlotte\FormBuilder\Core\FieldOption;
use JonasSlotte\FormBuilder\Core\FieldOptionCollection;
use JonasSlotte\FormBuilder\Core\FormType;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\FieldType;
use JonasSlotte\FormBuilder\Core\Form;
use JonasSlotte\FormBuilder\Core\Property;
use JonasSlotte\FormBuilder\Core\PropertyType;
use JonasSlotte\FormBuilder\Core\PropertyCollection;
use JonasSlotte\FormBuilder\Core\ValidationCollection;
use JonasSlotte\FormBuilder\FieldTypes\FieldTypeRegistry;
use JonasSlotte\FormBuilder\FormTypes\FormTypeRegistry;
use JonasSlotte\FormBuilder\Properties\PropertyTypeRegistry;
use JonasSlotte\FormBuilder\ValueTypes\ValueTypeRegistry;

/**
 * Configuration Service locator
 * Will reduce use calls that would result from the loose coupling.
 */
class FormBuilder
{
  /**
   * @var FormTypeRegistry
   */
  protected $formTypes;

  /**
   * @var FieldTypeRegistry
   */
  protected $fieldTypes;

  /**
   * @var ValueTypeRegistry
   */
  protected $valueTypes;

  /**
   * @var PropertyTypeRegistry
   */
  protected $propertyTypes;

  /**
   * @var array
   */
  protected $idCache = [];

  public function __construct()
  {
    $this->formTypes = app()->make(FormTypeRegistry::class);
    $this->fieldTypes = app()->make(FieldTypeRegistry::class);
    $this->valueTypes = app()->make(ValueTypeRegistry::class);
    $this->propertyTypes = app()->make(PropertyTypeRegistry::class);
  }

  /**
   * @param mixed $input
   * @return Id
   */
  public function id($input)
  {
    return new Id($input);
  }

  /**
   * @param string $fieldType
   * @return FieldType
   */
  public function fieldType(string $fieldType)
  {
    return $this->fieldTypes->get($fieldType);
  }

  /**
   * @param string $propertyType
   * @return PropertyType
   */
  public function propertyType(string $propertyType)
  {
    return $this->propertyTypes->get($propertyType);
  }

  /**
   * @param string $formType
   * @return FormType
   */
  public function formType(string $formType)
  {
    return $this->formTypes->get($formType);
  }

  /**
   * @param string $valueType
   * @return ValueType
   */
  public function valueType(string $valueType)
  {
    return $this->valueTypes->get($valueType);
  }

  /**
   * @param array $items
   * @return FieldCollection
   */
  public function fieldCollection($items = [])
  {
    $fields = [];
    foreach ($items ?? [] as $field) {
      $fields[] = $this->field($field);
    }
    return new FieldCollection($fields);
  }

  /**
   * @param array $items
   * @return PropertyCollection
   */
  public function propertyCollection($items = [])
  {
    $props = [];
    foreach ($items ?? [] as $key => $prop) {
      $props[] = $this->property($key, $prop);
    }
    return new PropertyCollection($props);
  }

  /**
   * @param array $items
   * @return FieldOptionCollection
   */
  public function fieldOptionCollection($items = [])
  {
    $options = [];
    foreach ($items ?? [] as $field) {
      $options[] = $this->fieldOption($field);
    }
    return new FieldOptionCollection($options);
  }

  public function extractStringValue(string $key, array $data, array $registry, string $prefix)
  {
    $value = $data[$key] ?? null;
    if ($value === null) {
      $value = $prefix . "_" . (sizeof($registry) + 1);
    }
    $registry[] = $value;
    return $value;
  }

  /**
   * @param array $data
   * @return Form
   */
  public function form(array $data)
  {
    $identity = new Id($this->extractStringValue("id", $data, $this->idCache, "form"));
    return new Form(
      $identity,
      $this->formType($data["type"] ?? "default"),
      $this->fieldCollection($data["fields"] ?? [])
    );
  }

  /**
   * @param array $data
   * @return Field
   */
  public function field(array $data)
  {
    $identity = new Id($this->extractStringValue("id", $data, $this->idCache, "field"));
    return new Field(
      $identity,
      $this->fieldType($data["type"]),
      $this->propertyCollection($data["properties"] ?? []),
      $this->fieldOptionCollection($data["options"] ?? [])
    );
  }

  /**
   * @param string $type
   * @param array $data
   * @return Field
   */
  public function property(string $type, array $data)
  {
    $identity = new Id($this->extractStringValue("id", $data, $this->idCache, "property"));
    return new Property(
      $identity,
      $this->propertyType($type),
      $data
    );
  }


  /**
   * @param array $data
   * @return FieldOption
   */
  public function fieldOption(array $data)
  {
    $identity = new Id($this->extractStringValue("id", $data, $this->idCache, "option"));
    return new FieldOption(
      $identity,
      $this->fieldCollection($data["fields"] ?? [])
    );
  }
}
