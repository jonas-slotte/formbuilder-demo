<?php

namespace JonasSlotte\FormBuilder\Services;

use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\FieldInstanceCollection;
use JonasSlotte\FormBuilder\Core\FormInstance;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\Parser;
use JonasSlotte\FormBuilder\ValueSources\ArrayValueSource;
use JonasSlotte\FormBuilder\ValueSources\MultiValueSource;
use JonasSlotte\FormBuilder\ValueSources\OldValueSource;
use JonasSlotte\FormBuilder\View\FormInstanceView;

/**
 * Higher-order builder
 */
class LaravelFormBuilder
{
  /**
   * Get the source used for session values
   *
   * @return OldValueSource
   */
  public function getSessionValueSource()
  {
    return new OldValueSource(app()->make('session.store'));
  }

  /**
   * Make vieweable form instance from array
   * with a single call
   * 
   * @param array $structure
   * @param array $data
   * @return FormInstanceView
   */
  public function form(array $structure, array $data = [])
  {
    /** @var FormBuilder $parser */
    $parser = new FormBuilder;
    $form = $parser->form($structure);
    $fieldInstances = new FieldInstanceCollection();
    $valueSource = new MultiValueSource();
    $defaultValueSource = new ArrayValueSource();
    foreach ($data as $valueId => $value) {
      $defaultValueSource->put(new Id($valueId), $value);
    }
    $sessionSource = $this->getSessionValueSource();
    $valueSource->register('session', $sessionSource);
    $valueSource->register('default', $defaultValueSource);
    foreach ($form->fields() as $field) {
      $fieldInstanceId = $field->id();
      $fieldInstances->push(new FieldInstance($field, $fieldInstanceId, $valueSource));
    }
    $formInstance = new FormInstance($form->id(), $form, $fieldInstances);
    return new FormInstanceView($formInstance);
  }
}
