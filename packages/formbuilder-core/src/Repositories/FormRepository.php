<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\FieldCollection;
use JonasSlotte\FormBuilder\Core\Form;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\FormTypes\FormTypeRegistry;
use JonasSlotte\FormBuilder\Models\Form as FormModel;

class FormRepository
{
    /** @var FormTypeRepository  */
    protected $formTypes;

    /** @var FieldRepository  */
    protected $fields;

    /** @var FormFieldRepository  */
    protected $formFields;

    /** @var FormTypeRegistry  */
    protected $typeRegistry;

    /**
     *
     */
    public function __construct(FormTypeRepository $formTypes, FieldRepository $fields, FormFieldRepository $formFields, FormTypeRegistry $typeRegistry)
    {
        $this->formTypes = $formTypes;
        $this->fields = $fields;
        $this->formFields = $formFields;
        $this->typeRegistry = $typeRegistry;
    }

    /**
     * @param mixed $id
     * @return Form
     */
    public function find(Id $id)
    {
        $formModel = FormModel::with([
            'formType',
            'fields.fieldType'
        ])->find($id);

        return $this->toDomainModel($formModel);
    }

    /**
     * @param FormModel $formModel
     * @return Form
     */
    public function toDomainModel(FormModel $formModel)
    {
        $fieldCollection = new FieldCollection();
        foreach ($formModel->fields as $fieldModel) {
            $fieldCollection->push($this->fields->toDomainModel($fieldModel));
        }

        return new Form(
            new Id($formModel->getKey()),
            $this->typeRegistry->get($formModel->formType->key),
            $fieldCollection
        );
    }


    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(Id $id)
    {
        return FormModel::where('id', $id)->exists();
    }

    /**
     * @param Form $form
     * @return void
     */
    public function persist(Form $form)
    {
        $model = FormModel::findOrNew($form->id());
        $model->id = $form->id();
        $model->formType()->associate($this->formTypes->find($form->type()));
        $model->save();

        $fields = $form->fields();
        foreach ($fields as $field) {
            $fieldModel = $this->fields->persist($field);
            $this->formFields->linkModels($model, $fieldModel);
        }
    }
}
