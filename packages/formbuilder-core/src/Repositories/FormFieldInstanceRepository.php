<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\Form;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Models\FormInstance as FormInstanceModel;
use JonasSlotte\FormBuilder\Models\Field as FieldModel;
use JonasSlotte\FormBuilder\Models\FormFieldInstance;
use JonasSlotte\FormBuilder\ValueResolvers\EloquentValueResolver;
use JonasSlotte\FormBuilder\ValueSources\ResolverValueSource;

class FormFieldInstanceRepository
{
    /**
     * @var FieldRepository
     */
    protected $fields;

    public function __construct(FieldRepository  $fields)
    {
        $this->fields = $fields;
    }

    /**
     * @param FieldInstance $field
     * @return FormFieldInstance
     */
    protected function firstOrNewSet(FieldInstance $field, FormInstanceModel $formModel)
    {
        $model = FormFieldInstance::findOrNew($field->id());
        $model->id = $field->id()->toInt();
        $model->formInstance()->associate($formModel);
        return $model;
    }

    public function persist(FieldInstance $instance, FormInstanceModel $formModel)
    {
        $model = $this->firstOrNewSet($instance, $formModel);
        $model->save();
    }

    public function toDomainModel(FormFieldInstance $instanceModel)
    {
        $valueResolver = new EloquentValueResolver($instanceModel);
        $field = $this->fields->find(new Id($instanceModel->formField->field_id));
        return new FieldInstance($field, new Id($instanceModel->id), new ResolverValueSource($valueResolver));
    }

    /**
     * @param mixed $id
     * @return FieldInstance
     */
    public function find(Id $id)
    {
        $model = FormFieldInstance::find($id);
        return $this->toDomainModel($model);
    }
}
