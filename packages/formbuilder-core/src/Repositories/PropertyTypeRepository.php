<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\PropertyType;
use JonasSlotte\FormBuilder\Models\PropertyType as PropertyTypeModel;

class PropertyTypeRepository
{
    /**
     * @param PropertyType $type
     * @return PropertyTypeModel
     */
    public function find(PropertyType $type)
    {
        return PropertyTypeModel::where('key', $type)->first();
    }

    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(PropertyType $type)
    {
        return PropertyTypeModel::where('key', $type)->exists();
    }
}
