<?php

namespace JonasSlotte\FormBuilder\Repositories;

use Exception;
use JonasSlotte\FormBuilder\Core\FormType;
use JonasSlotte\FormBuilder\Models\FormType as FormTypeModel;
use Illuminate\Support\Facades\DB;

class FormTypeRepository
{
    /**
     * @param FormType $type
     * @return FormTypeModel
     */
    public function find(FormType $type)
    {
        return FormTypeModel::where('key', $type)->firstOr(function () use ($type) {
            throw new Exception("Form Type '$type' does not exist");
        });
    }

    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(FormType $type)
    {
        return FormTypeModel::where('key', $type)->exists();
    }
}
