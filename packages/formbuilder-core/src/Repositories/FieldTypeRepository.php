<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\FieldType;
use JonasSlotte\FormBuilder\Models\FieldType as FieldTypeModel;
use Illuminate\Support\Facades\DB;

class FieldTypeRepository
{
    /**
     * @param FieldType $type
     * @return FieldTypeModel
     */
    public function find(FieldType $type)
    {
        return FieldTypeModel::where('key', $type)->first();
    }

    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(FieldType $type)
    {
        return FieldTypeModel::where('key', $type)->exists();
    }

    /**
     * Get a field type by string
     * 
     * @param string $key
     * @return FieldTypeModel|null
     */
    public function getType(string $key)
    {
        return  FieldTypeModel::where('key', $key)->first();
    }
}
