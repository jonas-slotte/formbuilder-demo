<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\Property;
use JonasSlotte\FormBuilder\Models\Property as PropertyModel;
use JonasSlotte\FormBuilder\Properties\PropertyTypeRegistry;

class PropertyRepository
{
    /** @var PropertyTypeRepository  */
    protected $propertyTypes;

    /** @var PropertyTypeRegistry  */
    protected $typeRegistry;

    public function __construct(PropertyTypeRepository $propertyTypes, PropertyTypeRegistry $typeRegistry)
    {
        $this->propertyTypes = $propertyTypes;
        $this->typeRegistry = $typeRegistry;
    }

    /**
     * @param mixed $id
     * @return Property
     */
    public function find(Id $id)
    {
        $model = PropertyModel::find($id);
        return $this->toDomainModel($model);
    }


    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(Id $id)
    {
        return PropertyModel::where('id', $id)->exists();
    }

    /**
     * @param Field $field
     * @return FieldModel
     */
    protected function firstOrNewSet(Property $prop)
    {
        $model = PropertyModel::findOrNew($prop->id());
        $model->id = $prop->id()->toInt();
        $model->propertyType()->associate($this->propertyTypes->find($prop->type()));
        return $model;
    }

    public function toDomainModel(PropertyModel $propertyModel)
    {
        return new Property(
            new Id($propertyModel->id),
            $this->typeRegistry->get($propertyModel->key),
            $propertyModel->data
        );
    }

    /**
     * Persist a Property as-is
     * 
     * @param Property
     * @return PropertyModel
     */
    public function persist(Property $field)
    {
        $model = $this->firstOrNewSet($field);
        $model->save();
        return $model;
    }
}
