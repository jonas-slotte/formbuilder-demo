<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\Form;
use JonasSlotte\FormBuilder\Models\Form as FormModel;
use JonasSlotte\FormBuilder\Models\Field as FieldModel;
use JonasSlotte\FormBuilder\Models\FormField as FormFieldModel;

class FormFieldRepository
{
    /**
     * Link a form and field
     *
     * @param Form $form
     * @param Field $field
     * @return void
     */
    public function link(Form $form, Field $field)
    {
        $formFieldModel = FormFieldModel::where('form_id', $form->id()->toInt())->where('field_id', $field->id()->toInt())->firstOrNew();
        $formFieldModel->form()->associate($form->id()->toInt());
        $formFieldModel->field()->associate($field->id()->toInt());
        $formFieldModel->save();
    }

    public function linkModels(FormModel $form, FieldModel $field)
    {
        $formFieldModel = FormFieldModel::where('form_id', $form->getKey())->where('field_id', $field->getKey())->firstOrNew();
        $formFieldModel->form()->associate($form);
        $formFieldModel->field()->associate($field);
        $formFieldModel->save();
    }

    /**
     * Unlink a form and field
     *
     * @param Form $form
     * @param Field $field
     * @return void
     */
    public function unlink(Form $form, Field $field)
    {
        FormFieldModel::where('form_id', $form->id()->toInt())->where('field_id', $field->id()->toInt())->delete();
    }

    public function unlinkModels(FormModel $form, FieldModel $field)
    {
        FormFieldModel::where('form_id', $form->getKey())->where('field_id', $field->getKey())->delete();
    }
}
