<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\FieldInstanceCollection;
use JonasSlotte\FormBuilder\Core\FormInstance;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Models\FormInstance as FormInstanceModel;

class FormInstanceRepository
{
    /**
     * @var FormFieldInstanceRepository
     */
    protected $fields;

    /**
     * @var FormRepository
     */
    protected $forms;

    public function __construct(FormFieldInstanceRepository $fields, FormRepository $forms)
    {
        $this->fields = $fields;
        $this->forms = $forms;
    }
    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(Id $id)
    {
        return FormInstanceModel::where('id', $id)->exists();
    }

    protected function toDomainModel(FormInstanceModel $instanceModel)
    {
        $form = $this->forms->find(new Id($instanceModel->form_id));
        $fieldInstanceCollection = new FieldInstanceCollection();
        foreach ($instanceModel->formFieldInstances as $fieldInstance) {
            $fieldInstanceCollection->push($this->fields->toDomainModel($fieldInstance));
        }

        return new FormInstance(
            new Id($instanceModel->getKey()),
            $form,
            $fieldInstanceCollection
        );
    }

    /**
     * @param mixed $id
     * @return FormInstance
     */
    public function find(Id $id)
    {
        $model = FormInstanceModel::find($id);
        return $this->toDomainModel($model);
    }

    /**
     * @param FormInstance $form
     * @return void
     */
    public function persist(FormInstance $form)
    {
        $model = FormInstanceModel::findOrNew($form->id());
        $model->id = $form->id();
        $model->form()->associate($form->form()->id()->get());
        $model->save();

        $fields = $form->fields();
        foreach ($fields as $field) {
            $fieldModel = $this->fields->persist($field, $model);
        }
    }
}
