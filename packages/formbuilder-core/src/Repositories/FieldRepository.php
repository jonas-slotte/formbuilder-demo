<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldOption;
use JonasSlotte\FormBuilder\Core\FieldOptionCollection;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Core\ValidationCollection;
use JonasSlotte\FormBuilder\Models\Field as FieldModel;
use JonasSlotte\FormBuilder\FieldTypes\FieldTypeRegistry;

class FieldRepository
{
    /** @var FieldTypeRepository  */
    protected $fieldTypes;

    /** @var OptionsRepository  */
    protected $options;

    /** @var FieldTypeRegistry  */
    protected $typeRegistry;

    /**
     *
     */
    public function __construct(FieldTypeRepository $fieldTypes, FieldTypeRegistry $typeRegistry)
    {
        $this->fieldTypes = $fieldTypes;
        $this->options = new OptionsRepository($fieldTypes, $this);
        $this->typeRegistry = $typeRegistry;
    }

    /**
     * @param mixed $id
     * @return Field
     */
    public function find(Id $id)
    {
        $model = FieldModel::find($id);
        return $this->toDomainModel($model);
    }


    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(Id $id)
    {
        return FieldModel::where('id', $id)->exists();
    }

    /**
     * @param Field $field
     * @return FieldModel
     */
    protected function firstOrNewSet(Field $field)
    {
        $model = FieldModel::findOrNew($field->id());
        $model->id = $field->id()->toInt();
        $model->fieldType()->associate($this->fieldTypes->find($field->type()));
        return $model;
    }

    public function toDomainModel(FieldModel $fieldModel)
    {
        $fieldOptions = new FieldOptionCollection();
        foreach ($fieldModel->childOptions as $optionModel) {
            $fieldOptions->push($this->options->toDomainModel($optionModel));
        }
        return new Field(
            new Id($fieldModel->id),
            $this->typeRegistry->get($fieldModel->fieldType->key),
            new ValidationCollection([]),
            $fieldOptions
        );
    }

    /**
     * Persist a field as-is without parent
     * 
     * @param Field $field
     * @return FieldModel
     */
    public function persist(Field $field)
    {
        $model = $this->firstOrNewSet($field);
        $model->save();
        $this->afterFieldSave($field, $model);

        return $model;
    }

    /**
     * Persist a field as a child to another field
     *
     * @param Field $field
     * @param FieldOption $opt
     * @return FieldModel
     */
    public function persistOptionChild(Field $field, FieldModel $optmodel)
    {
        $model = $this->firstOrNewSet($field);
        $model->parentField()->associate($optmodel);
        $model->save();

        $this->afterFieldSave($field, $model);

        return $model;
    }

    protected function afterfieldSave(Field $field, FieldModel $model)
    {
        $options = $field->options();
        foreach ($options as $option) {
            $this->options->persistFieldOption($option, $model);
        }
    }
}
