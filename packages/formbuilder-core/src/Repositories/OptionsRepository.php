<?php

namespace JonasSlotte\FormBuilder\Repositories;

use JonasSlotte\FormBuilder\Core\FieldCollection;
use JonasSlotte\FormBuilder\Core\FieldOption;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Models\Field as FieldModel;

class OptionsRepository
{
    /** @var FieldTypeRepository  */
    protected $fieldTypes;

    /** @var FieldRepository  */
    protected $fields;

    /**
     *
     */
    public function __construct(FieldTypeRepository $fieldTypes, FieldRepository $fields)
    {
        $this->fieldTypes = $fieldTypes;
        $this->fields = $fields;
    }


    /**
     * @param mixed $id
     * @return FieldModel
     */
    public function find(Id $id)
    {
        return FieldModel::fieldTypeKey('option')->find($id);
    }


    /**
     * @param mixed $id
     * @return boolean
     */
    public function exists(Id $id)
    {
        return FieldModel::fieldTypeKey('option')->where('id', $id)->exists();
    }

    /**
     * @param FieldOption $opt
     * @return FieldModel
     */
    public function persistFieldOption(FieldOption $opt, FieldModel $parent)
    {
        $model = FieldModel::fieldTypeKey('option')->findOrNew($opt->id());
        $model->id = $opt->id()->toInt();
        $model->parentField()->associate($parent);
        $model->fieldType()->associate($this->fieldTypes->getType('option'));
        $model->save();

        $fields = $opt->fields();
        foreach ($fields as $field) {
            $child = $this->fields->persistOptionChild($field, $model);
        }

        return $model;
    }

    /**
     * @param FieldModel $optionModel
     * @return FieldOption
     */
    public function toDomainModel(FieldModel $optionModel)
    {
        $fieldCollection = new FieldCollection();
        foreach ($optionModel->childFields as $childField) {
            $fieldCollection->push($this->fields->toDomainModel($childField));
        }
        return new FieldOption(
            new Id($optionModel->id),
            $fieldCollection
        );
    }
}
