<?php

namespace JonasSlotte\FormBuilder\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property FormInstance|null $formInstance
 * @property StringValue|null $stringValue
 * @property TextValue|null $textValue
 * @property BooleanValue|null $booleanValue
 * @property DateTimeValue|null $dateTimeValue
 * @property IntegerValue|null $integerValue
 * @property DateValue|null $dateValue
 * @property TimeValue|null $timeValue
 * @property FormField|null $formField
 */
class FormFieldInstance extends Model
{
    use HasFactory;

    public function formInstance()
    {
        return $this->belongsTo(FormInstance::class);
    }

    public function formField()
    {
        return $this->belongsTo(FormField::class);
    }

    public function stringValue()
    {
        return $this->hasOne(StringValue::class);
    }

    public function textValue()
    {
        return $this->hasOne(TextValue::class);
    }

    public function booleanValue()
    {
        return $this->hasOne(BooleanValue::class);
    }

    public function dateTimeValue()
    {
        return $this->hasOne(DateTimeValue::class);
    }

    public function integerValue()
    {
        return $this->hasOne(IntegerValue::class);
    }

    public function dateValue()
    {
        return $this->hasOne(DateValue::class);
    }

    public function timeValue()
    {
        return $this->hasOne(TimeValue::class);
    }
}
