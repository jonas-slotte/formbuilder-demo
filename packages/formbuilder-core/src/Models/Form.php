<?php

namespace JonasSlotte\FormBuilder\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    use HasFactory;

    public function formType()
    {
        return $this->belongsTo(FormType::class);
    }

    public function fields()
    {
        // Country.php
        // return $this->hasManyThrough(
        //     'App\Models\Post',
        //     'App\Models\User',
        //     'country_id', // Foreign key on users table...
        //     'user_id', // Foreign key on posts table...
        //     'id', // Local key on countries table...
        //     'id' // Local key on users table...
        // );
        return $this->hasManyThrough(
            Field::class,
            FormField::class,
            'form_id',
            'id',
            'id',
            'field_id'
        );
    }
}
