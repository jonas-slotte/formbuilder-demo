<?php

namespace JonasSlotte\FormBuilder\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array'
    ];

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }

    public function propertyType()
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    public function scopePropertyTypeKey(Builder $q, string $key)
    {
        return $q->whereHas('propertyType', function (Builder $w) use ($key) {
            return $w->where('key', $key);
        });
    }
}
