<?php

namespace JonasSlotte\FormBuilder\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    use HasFactory;

    public function fieldType()
    {
        return $this->belongsTo(FieldType::class);
    }

    public function parentField()
    {
        return $this->belongsTo(Field::class, 'parent_field_id');
    }

    public function childFields()
    {
        return $this->hasMany(Field::class, 'parent_field_id');
    }

    public function childOptions()
    {
        return $this->childFields()->fieldTypeKey('option');
    }

    public function scopeFieldTypeKey(Builder $q, string $key)
    {
        return $q->whereHas('fieldType', function (Builder $w) use ($key) {
            return $w->where('key', $key);
        });
    }
}
