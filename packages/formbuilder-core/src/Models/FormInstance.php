<?php

namespace JonasSlotte\FormBuilder\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormInstance extends Model
{
    use HasFactory;

    public function formFieldInstances()
    {
        return $this->belongsTo(FormFieldInstance::class);
    }
}
