<?php

namespace JonasSlotte\FormBuilder\View;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\Form;
use JonasSlotte\FormBuilder\Core\FormInstance;
use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\ValueSources\ArrayValueSource;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class FormInstanceView
{
  /**
   * @var FormInstance
   */
  protected $instance;

  public function __construct(FormInstance $instance)
  {
    $this->instance = $instance;
  }

  public function name()
  {
    return $this->instance->form()->name()->get();
  }

  public function id()
  {
    return $this->instance->id();
  }

  /**
   * @return FieldInstanceView[]
   */
  public function fieldViews()
  {
    return $this->instance->fields()->map(function (FieldInstance $e) {
      return $e->field()->type()->makeView($e);
    });
  }

  public function makeValidator(array $data)
  {
    $rules = [];
    $attributes = [];
    foreach ($this->fieldViews() as $fieldView) {
      $rules[$fieldView->getInputName()] = $fieldView->rules();
      $attributes[$fieldView->getInputName()] = $fieldView->name();
    }
    return Validator::make($data, $rules, [], $attributes);
  }

  public function sanitizeInput(array $data)
  {
    $values = [];
    foreach ($this->fieldViews() as $fieldView) {
      $values[$fieldView->getInputName()] = $data[$fieldView->getInputName()] ?? null;
    }
    return $values;
  }

  public function validate(array $data)
  {
    $this->makeValidator($data)->validate();
  }
}
