<?php

namespace JonasSlotte\FormBuilder\View;

use JonasSlotte\FormBuilder\Core\FieldInstance;

abstract class FieldInstanceView
{
  /**
   * @var FieldInstance
   */
  protected $instance;

  public function __construct(FieldInstance $instance)
  {
    $this->instance = $instance;
  }

  public function field()
  {
    return $this->instance->field();
  }

  public function instance()
  {
    return $this->instance;
  }

  public function name()
  {
    return $this->field()->name();
  }

  public function id()
  {
    return $this->instance()->id();
  }

  public function htmlAttributes()
  {
    return [];
  }

  public function errors()
  {
    return [];
  }

  public function rules()
  {
    return [];
  }

  public function getInputName()
  {
    return "";
  }

  public function getId()
  {
    return "";
  }

  public function getValue()
  {
    return "";
  }
}
