<?php

namespace JonasSlotte\FormBuilder\ValueResolvers;

use JonasSlotte\FormBuilder\Models\FormFieldInstance;
use JonasSlotte\FormBuilder\Models\StringValue;

class EloquentValueResolver extends ValueResolver
{
  /**
   * @var FormFieldInstance
   */
  protected $instance;

  public function __construct(FormFieldInstance $instance)
  {
    $this->instance = $instance;
  }
  /**
   * @param FormFieldInstance $instance
   * @return string
   */
  public function getStringValue()
  {
    $instance = $this->instance;
    return $instance->stringValue ? $instance->stringValue->getValue() : "";
  }

  /**
   * @param FormFieldInstance $instance
   * @param string
   * @return void
   */
  public function persistStringValue(string $value)
  {
    $instance = $this->instance;
    if (!$instance->stringValue) {
      $stringValue = new StringValue();
      $stringValue->formFieldInstance()->associate($instance);
    }
    $stringValue->value($value);
    $stringValue->save();
  }
}
