<?php

namespace JonasSlotte\FormBuilder\ValueResolvers;



class ValueResolver
{

  /**
   * @param FormFieldInstance $instance
   * @return string
   */
  public function getStringValue()
  {
  }

  /**
   * @param string
   * @return void
   */
  public function persistStringValue(string $value)
  {
  }
}
