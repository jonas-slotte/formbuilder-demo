<?php

namespace JonasSlotte\FormBuilder\FormTypes;

use JonasSlotte\FormBuilder\Core\FormType;

class FormTypeRegistry
{
  /**
   * @var array
   */
  protected $types = [];

  public function registerClass(string $key, string $class)
  {
    $this->register($key, new $class($key));
  }

  public function register(string $key, FormType $type)
  {
    $this->types[$key] = $type;
  }

  /**
   * @param string $key
   * @return FormType
   */
  public function get(string $key)
  {
    return $this->types[$key];
  }
}
