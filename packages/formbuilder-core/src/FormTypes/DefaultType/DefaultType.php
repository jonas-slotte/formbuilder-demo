<?php

namespace JonasSlotte\FormBuilder\FormTypes\DefaultType;

use JonasSlotte\FormBuilder\Core\Form;
use JonasSlotte\FormBuilder\Core\FormType;
use JonasSlotte\FormBuilder\View\FormView;

class DefaultType extends FormType
{
  public function makeView(Form $form)
  {
    return new DefaultView($form);
  }
}
