<?php

namespace JonasSlotte\FormBuilder\FieldTypes;

use JonasSlotte\FormBuilder\Core\FieldType;

class FieldTypeRegistry
{
  /**
   * @var array
   */
  protected $fields = [];

  public function register(string $key, FieldType $type)
  {
    $this->fields[$key] = $type;
  }

  public function registerClass(string $key, string $class)
  {
    $this->register($key, new $class($key));
  }

  /**
   * @param string $key
   * @return FieldType
   */
  public function get(string $key)
  {
    return $this->fields[$key];
  }
}
