<?php

namespace JonasSlotte\FormBuilder\FieldTypes\Email;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\View\FieldInstanceView;

class EmailView extends FieldInstanceView
{
  public function bladeKey()
  {
    return 'formbuilder.' . $this->field()->type();
  }

  public function rules()
  {
    return $this->field()->type()->defaultRules();
  }

  public function getInputName()
  {
    return $this->instance()->id()->get();
  }

  public function getId()
  {
    return $this->instance()->id()->get();
  }

  public function getValue()
  {
    return $this->instance()->valueSource()->getString($this->id());
  }

  public function htmlAttributes()
  {
    return [
      'id' => $this->getId(),
      'name' => $this->getInputName(),
      'value' => $this->getValue(),
      'type' => 'email'
    ];
  }
}
