<?php

namespace JonasSlotte\FormBuilder\FieldTypes\Email;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\FieldType;
use JonasSlotte\FormBuilder\Core\ValueSource;

class EmailType extends FieldType
{
  public function makeView(FieldInstance $inst)
  {
    return new EmailView($inst);
  }

  public function defaultRules()
  {
    return [
      'required',
      'string',
      'email',
      'max:255'
    ];
  }
}
