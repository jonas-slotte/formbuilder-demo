<?php

namespace JonasSlotte\FormBuilder\FieldTypes\Text;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\FieldType;
use JonasSlotte\FormBuilder\Core\ValueSource;

class TextType extends FieldType
{
  public function makeView(FieldInstance $inst)
  {
    return new TextView($inst);
  }

  public function defaultRules()
  {
    return [
      'required',
      'string',
      'max:255'
    ];
  }
}
