<?php

namespace JonasSlotte\FormBuilder\FieldTypes\Option;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\FieldType;
use JonasSlotte\FormBuilder\Core\ValueSource;

class OptionType extends FieldType
{
  public function makeView(FieldInstance $inst)
  {
    return new OptionView($inst);
  }

  public function defaultRules()
  {
    return [
      'nullable',
    ];
  }
}
