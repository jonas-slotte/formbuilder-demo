<?php

namespace JonasSlotte\FormBuilder\FieldTypes\TextArea;

use JonasSlotte\FormBuilder\Core\Field;
use JonasSlotte\FormBuilder\Core\FieldInstance;
use JonasSlotte\FormBuilder\Core\FieldType;
use JonasSlotte\FormBuilder\Core\ValueSource;

class TextAreaType extends FieldType
{
  public function makeView(FieldInstance $inst)
  {
    return new TextAreaView($inst);
  }

  public function defaultRules()
  {
    return [
      'required',
      'string',
      'max:1000'
    ];
  }
}
