<?php

namespace JonasSlotte\FormBuilder;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use JonasSlotte\FormBuilder\FieldTypes\Email\EmailType;
use JonasSlotte\FormBuilder\Repositories\FieldRepository;
use JonasSlotte\FormBuilder\Repositories\FieldTypeRepository;
use JonasSlotte\FormBuilder\Repositories\FormFieldRepository;
use JonasSlotte\FormBuilder\Repositories\FormRepository;
use JonasSlotte\FormBuilder\Repositories\FormTypeRepository;
use JonasSlotte\FormBuilder\FieldTypes\FieldTypeRegistry;
use JonasSlotte\FormBuilder\FormTypes\FormTypeRegistry;
use JonasSlotte\FormBuilder\FieldTypes\Text\TextType;
use JonasSlotte\FormBuilder\FieldTypes\Option\OptionType;
use JonasSlotte\FormBuilder\FieldTypes\TextArea\TextAreaType;
use JonasSlotte\FormBuilder\FormTypes\DefaultType\DefaultType;
use JonasSlotte\FormBuilder\Repositories\ValueRepository;
use JonasSlotte\FormBuilder\Services\FormBuilder;
use JonasSlotte\FormBuilder\ValueTypes\StringType;
use JonasSlotte\FormBuilder\ValueTypes\ValueTypeRegistry;
use JonasSlotte\FormBuilder\Properties\Key\KeyProperty;
use JonasSlotte\FormBuilder\Properties\Name\NameProperty;
use JonasSlotte\FormBuilder\Properties\PropertyTypeRegistry;
use JonasSlotte\FormBuilder\Repositories\PropertyRepository;

class FormBuilderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerRepositories();
        $this->registerFieldTypes();
        $this->registerFormTypes();
        $this->registerValueTypes();
        $this->registerProperties();

        $this->app->singleton(FormBuilder::class, function ($app) {
            return new FormBuilder();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Filesystem $filesystem)
    {
        $this->publishes([
            __DIR__ . '/../database/migrations/create_formbuilder_tables.php' => $this->getMigrationFileName($filesystem),
        ], 'migrations');
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path . '*_create_formbuilder_tables.php');
            })->push($this->app->databasePath() . "/migrations/{$timestamp}_create_formbuilder_tables.php")
            ->first();
    }

    protected function registerProperties()
    {
        $registry = $this->app[PropertyTypeRegistry::class];
        $registry->registerClass('name', NameProperty::class);
        $registry->registerClass('key', KeyProperty::class);
        // $registry->registerClass('validation', ValidationProperty::class);
    }


    protected function registerValueTypes()
    {
        $registry = $this->app[ValueTypeRegistry::class];
        $registry->registerClass('string', StringType::class);
    }

    protected function registerFieldTypes()
    {
        $registry = $this->app[FieldTypeRegistry::class];
        $registry->registerClass('text', TextType::class);
        $registry->registerClass('option', OptionType::class);
        $registry->registerClass('textarea', TextAreaType::class);
        $registry->registerClass('email', EmailType::class);
    }

    protected function registerFormTypes()
    {
        $registry = $this->app[FormTypeRegistry::class];
        $registry->registerClass('default', DefaultType::class);
    }

    protected function registerRepositories()
    {
        $this->app->singleton(PropertyTypeRegistry::class, function ($app) {
            return new PropertyTypeRegistry();
        });
        $this->app->singleton(ValueTypeRegistry::class, function ($app) {
            return new ValueTypeRegistry();
        });
        $this->app->singleton(FormTypeRegistry::class, function ($app) {
            return new FormTypeRegistry();
        });
        $this->app->singleton(FieldTypeRegistry::class, function ($app) {
            return new FieldTypeRegistry();
        });
        $this->app->singleton(FormFieldRepository::class, function ($app) {
            return new FormFieldRepository();
        });
        $this->app->singleton(FieldTypeRepository::class, function ($app) {
            return new FieldTypeRepository();
        });
        $this->app->singleton(PropertyRepository::class, function ($app) {
            return new PropertyRepository($app[PropertyTypeRepository::class], $app[PropertyTypeRegistry::class]);
        });
        $this->app->singleton(FieldRepository::class, function ($app) {
            return new FieldRepository($app[FieldTypeRepository::class], $app[FieldTypeRegistry::class]);
        });
        $this->app->singleton(FormTypeRepository::class, function ($app) {
            return new FormTypeRepository();
        });
        $this->app->singleton(FormRepository::class, function ($app) {
            return new FormRepository(
                $app[FormTypeRepository::class],
                $app[FieldRepository::class],
                $app[FormFieldRepository::class],
                $app[FormTypeRegistry::class]
            );
        });
        $this->app->singleton(ValueRepository::class, function ($app) {
            return new ValueRepository();
        });
    }
}
