<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormBuilderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * The customizable field type table
         */
        Schema::create('field_types', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->timestamps();
        });
        /**
         * The customizable form type table
         */
        Schema::create('form_types', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->timestamps();
        });
        /**
         * The customizable property type table
         */
        Schema::create('property_types', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->timestamps();
        });

        /**
         * The top-level field definitions
         */
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_field_id')->nullable();
            $table->unsignedBigInteger('field_type_id');
            $table->timestamps();

            $table->foreign('field_type_id')->references('id')->on('field_types');
            $table->foreign('parent_field_id')->references('id')->on('fields');
        });
        /**
         * The property maps to fields
         */
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('field_id')->nullable();
            $table->unsignedBigInteger('property_type_id');
            $table->json('data');
            $table->timestamps();

            $table->foreign('field_id')->references('id')->on('fields');
            $table->foreign('property_type_id')->references('id')->on('property_types');
        });

        /**
         * The top-level form structure aggregates
         */
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_type_id');
            $table->timestamps();

            $table->foreign('form_type_id')->references('id')->on('form_types');
        });
        /**
         * The link between forms and fields
         */
        Schema::create('form_fields', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_id');
            $table->unsignedBigInteger('field_id');
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('forms');
            $table->foreign('field_id')->references('id')->on('fields');
        });

        $this->migrateInstanceTables();
    }

    /**
     * Tables for instances
     *
     * @return void
     */
    protected function migrateInstanceTables()
    {
        /**
         * Concrete storage of a form's data
         */
        Schema::create('form_instances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_id');
            $table->timestamps();

            $table->foreign('form_id')->references('id')->on('forms');
        });

        /**
         * Concrete storage of a form instance's field.
         * This is the root element for all value storage.
         */
        Schema::create('form_field_instances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('form_field_id');
            $table->unsignedBigInteger('form_instance_id');
            $table->timestamps();

            $table->foreign('form_field_id')->references('id')->on('form_fields');
            $table->foreign('form_instance_id')->references('id')->on('form_instances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_field_instances');
        Schema::dropIfExists('form_instances');

        Schema::dropIfExists('form_fields');

        Schema::dropIfExists('properties');
        Schema::dropIfExists('fields');
        Schema::dropIfExists('forms');

        Schema::dropIfExists('property_types');
        Schema::dropIfExists('field_types');
        Schema::dropIfExists('form_types');
    }
}
