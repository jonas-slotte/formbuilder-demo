<?php

namespace JonasSlotte\FormBuilder\Tests\Unit;

use JonasSlotte\FormBuilder\Core\FormType;
use JonasSlotte\FormBuilder\FieldTypes\FieldTypeRegistry;
use JonasSlotte\FormBuilder\FieldTypes\Text\TextType;
use JonasSlotte\FormBuilder\FormTypes\FormTypeRegistry;
use JonasSlotte\FormBuilder\Services\FormBuilder;
use JonasSlotte\FormBuilder\Tests\TestCase;

class ParserTest extends TestCase
{
    /**
     * Test it passes the parser
     * 
     * @return void
     */
    public function testParser()
    {
        $formTypes = $this->app[FormTypeRegistry::class];
        $formTypes->register('formtype', new FormType('formtype'));
        $fieldTypes =  $this->app[FieldTypeRegistry::class];
        $fieldTypes->register('fieldtype', new TextType('fieldtype'));
        $fieldTypes->register('optionfieldtype', new TextType('optionfieldtype'));

        $parser = new FormBuilder();
        $form = $parser->form([
            'id' => "formid",
            'key' => 'testform',
            'name' => 'formname',
            'type' => 'formtype',
            'fields' => [
                [
                    'id' => 'fieldid',
                    'key' => 'testfield',
                    'name' => 'fieldname',
                    'type' => 'fieldtype',
                    'options' => [
                        [
                            'id' => 'optionid',
                            'key' => 'optionkey',
                            'name' => 'optionname',
                            'fields' => [
                                [
                                    'id' => 'optionfieldid',
                                    'key' => 'optionfield',
                                    'name' => 'optionfieldname',
                                    'type' => 'optionfieldtype'
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        $this->assertEquals('formid', $form->id()->get());
        $this->assertEquals('formtype', $form->type()->get());

        $this->assertEquals(1, sizeof($form->fields()));
        $this->assertEquals('fieldid', $form->fields()->get(0)->id());
        $this->assertEquals('optionid', $form->fields()->get(0)->options()->get(0)->id());
        $this->assertEquals('optionfieldid', $form->fields()->get(0)->options()->get(0)->fields()->get(0)->id());
    }
}
