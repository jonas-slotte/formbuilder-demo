<?php

namespace Tests\Unit;

use JonasSlotte\FormBuilder\Core\Id;
use JonasSlotte\FormBuilder\Tests\TestCase as TestsTestCase;
use JonasSlotte\FormBuilder\ValueSources\ArrayValueSource;
use JonasSlotte\FormBuilder\ValueSources\MultiValueSource;
use Tests\TestCase;

class ValueSourceTest extends TestsTestCase
{
  public function testMultiValueSourcePriority()
  {
    $multi = new MultiValueSource();
    $zero = new ArrayValueSource();
    $one = new ArrayValueSource();
    $two = new ArrayValueSource();
    $id = new Id("one");
    $multi->register('one', $one);
    $multi->register('two', $two);

    // $zero->put($id, "");
    $one->put($id, "theone");
    $two->put($id, "thetwo");

    $this->assertEquals(null, $zero->get($id));
    $this->assertEquals("theone", $one->get($id));
    $this->assertEquals("thetwo", $two->get($id));

    $this->assertEquals("theone", $multi->get($id), "Should be the first source's value");
  }
}
