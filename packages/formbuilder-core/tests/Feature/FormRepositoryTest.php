<?php

namespace JonasSlotte\FormBuilder\Tests\Feature;

use JonasSlotte\FormBuilder\FormTypes\DefaultType\DefaultType;
use JonasSlotte\FormBuilder\FormTypes\FormTypeRegistry;
use JonasSlotte\FormBuilder\Repositories\FormRepository;
use JonasSlotte\FormBuilder\Services\FormBuilder;
use JonasSlotte\FormBuilder\Tests\TestCase;
use JonasSlotte\FormBuilder\Tests\TestSeeder;

class FormRepositoryTest extends TestCase
{
    /**
     * @return FormRepository
     */
    protected function makeRepository()
    {
        $this->seed(TestSeeder::class);
        return app()->make(FormRepository::class);
    }
    /**
     * @return void
     */
    public function testPersist()
    {
        /**
         * Use formbuilder to make a somewhat complex form we can persist
         */
        $builder = new FormBuilder();

        $form = $builder->form([
            'id' => 1,
            "name" => "formname",
            "key" => "formkey",
            "version" => 1,
            "fields" => [
                [
                    'id' => 3,
                    "name" => "fieldname",
                    "key" => "fieldkey",
                    "version" => 1,
                    "type" => "option",
                    "options" => [
                        [
                            'id' => 2,
                            "name" => "fieldoption",
                            "key" => "fieldoptionkey",
                            "version" => 1,
                            "fields" => [
                                [
                                    'id' => 1,
                                    "name" => "fieldname",
                                    "key" => "fieldkey",
                                    "version" => 1,
                                    "type" => "text",
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]);

        // $formFields = $resolver->fieldCollection([
        //     $formField
        // ]);
        // $form = new Form(
        //     $resolver->identity(1, "formname", "formkey", 1),
        //     $resolver->formType('default'),
        //     $formFields
        // );



        $repo = $this->makeRepository();
        $repo->persist($form);

        $formField = $form->fields()->first();
        $fieldOption = $formField->options()->first();
        $optionField = $fieldOption->fields()->first();

        $this->assertDatabaseHas('forms', [
            'id' => $form->id()->get(),
        ]);
        $this->assertDatabaseHas('fields', [
            'id' => $formField->id()->get(),
        ]);
        $this->assertDatabaseHas('form_fields', [
            'form_id' => $form->id()->toInt(),
            'field_id' => $formField->id()->toInt(),
        ]);
        $this->assertDatabaseHas('fields', [
            'id' => $fieldOption->id()->toInt(),
            'parent_field_id' => $formField->id()->toInt()
        ]);

        //Assert that a re-fetched intance is complete (ids)
        $found = $repo->find($form->id());
        $this->assertEquals(true, $found->is($form));
        $foundField = $found->fields()->first();
        $this->assertEquals(true, $foundField->is($formField));
        $foundOption = $foundField->options()->first();
        $this->assertEquals(true, $foundOption->is($fieldOption));
        $foundOptionField = $foundOption->fields()->first();
        $this->assertEquals(true, $foundOptionField->is($optionField));
    }
}
