<div class="form-group">
    @include('formbuilder.components.label')
    <input class="form-control" @foreach ($field->htmlAttributes() as $attribute => $attributeValue)
    {!! $attribute . '="' . $attributeValue . '"' !!}
    @endforeach>
    @include('formbuilder.components.errors')
</div>