<div class="form-group">
    @include('formbuilder.components.label')
    <textarea class="form-control" @foreach ($field->htmlAttributes() as $attribute => $attributeValue)
    {!! $attribute . '="' . $attributeValue . '"' !!}
    @endforeach>{{$field->getValue()}}</textarea>
    @include('formbuilder.components.errors')
</div>