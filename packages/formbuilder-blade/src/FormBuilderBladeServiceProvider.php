<?php

namespace JonasSlotte\FormBuilderBlade;

use Illuminate\Support\ServiceProvider as Base;
use Illuminate\Support\Facades\Blade;

class FormBuilderBladeServiceProvider extends Base
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'formbuilder');
        $this->publishes([
            __DIR__ . '/../views' => resource_path('views/vendor/formbuilder'),
        ]);
        // Blade::componentNamespace('JonasSlotte\\FormBuilderBlade\\Views\\Components', 'formbuilder');
    }
}
